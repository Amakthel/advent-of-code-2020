#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int byrcheck(char string[10]){
  char *trash;
  if (string[4] != ' ') return 0;
  if (string[3] == ' ') return 0;
  if ((strtol(string, &trash, 10) >= 1920) && (strtol(string, &trash, 10) <= 2002)) return 1;
  return 0;
}
int iyrcheck(char string[10]){
  char *trash;
  if (string[4] != ' ') return 0;
  if (string[3] == ' ') return 0;
  if ((strtol(string, &trash, 10) >= 2010) && (strtol(string, &trash, 10) <= 2020)) return 1;
  return 0;
}
int eyrcheck(char string[10]){
  char *trash;
  if (string[4] != ' ') return 0;
  if (string[3] == ' ') return 0;
  if ((strtol(string, &trash, 10) >= 2020) && (strtol(string, &trash, 10) <= 2030)) return 1;
  return 0;
}
int hgtcheck(char string[10]){
  char *unit;
  int amount = strtol(string, &unit, 10);
  if ((strncmp(unit, "in ", 3) == 0) && (amount >= 59) && (amount <= 76)) return 1;
  if ((strncmp(unit, "cm ", 3) == 0) && (amount >= 150) && (amount <= 193)) return 1;
  return 0;
}
int hclcheck(char string[10]){
  int i;
  if (string[0] != '#') return 0;
  if (string[7] != ' ') return 0;
  if (string[6] == ' ') return 0;
  for (i=1;i<7;++i){
    if (string[i] < '0') return 0;
    if ((string[i] > '9') && (string[i] < 'a')) return 0;
    if (string[i] > 'f') return 0;
  }
  return 1;
}
int eclcheck(char string[10]){
  if (strncmp(string, "amb", 3) == 0) return 1;
  if (strncmp(string, "blu", 3) == 0) return 1;
  if (strncmp(string, "brn", 3) == 0) return 1;
  if (strncmp(string, "gry", 3) == 0) return 1;
  if (strncmp(string, "grn", 3) == 0) return 1;
  if (strncmp(string, "hzl", 3) == 0) return 1;
  if (strncmp(string, "oth", 3) == 0) return 1;
  return 0;
}
int pidcheck(char string[10]){
  int i;
  if (string[9] != ' ') return 0;
  if (string[8] == ' ') return 0;
  for (i=0;i<9;++i){
    if ((string[i] < '0') || (string[i] > '9')) return 0;
  }
  return 1;
}

int main(){
  int c, total, i;
  char previous[] = "   ";
  char passport[] = "       ";
  char value[] = "          ";

  /* byr birth year       passport[0]
   * iyr issue year       passport[1]
   * eyr expiration year  passport[2]
   * hgt height           passport[3]
   * hcl hair color       passport[4]
   * ecl eye color        passport[5]
   * pid passport id      passport[6]
   * cid country id       unimportant
   */

  total = 0;
  while ((c = getchar()) != EOF){
    if (strncmp(passport, "xxxxxxx", 7) == 0) {
      ++total;
      strcpy(passport, "       ");
    }

    if ((c == '\n') && (previous[2] == '\n')) {
      strcpy(passport, "       ");
    }
    
    if (c == ':'){
      i = 0;
      while (((c = getchar()) != ' ') && (c != '\n')){
        if (i == 10) break;
        value[i] = c;
        ++i;
      }
      i = 0;

      if ((strncmp(previous, "byr", 3) == 0) && byrcheck(value)) passport[0] = 'x';
      if ((strncmp(previous, "iyr", 3) == 0) && iyrcheck(value)) passport[1] = 'x';
      if ((strncmp(previous, "eyr", 3) == 0) && eyrcheck(value)) passport[2] = 'x';
      if ((strncmp(previous, "hgt", 3) == 0) && hgtcheck(value)) passport[3] = 'x';
      if ((strncmp(previous, "hcl", 3) == 0) && hclcheck(value)) passport[4] = 'x';
      if ((strncmp(previous, "ecl", 3) == 0) && eclcheck(value)) passport[5] = 'x';
      if ((strncmp(previous, "pid", 3) == 0) && pidcheck(value)) passport[6] = 'x';

      strcpy(value, "          ");
    }

    previous[0] = previous[1];
    previous[1] = previous[2];
    previous[2] = c;
  }
  
  printf("the number of valid passports is %d\n", total);
  return 0;
}
