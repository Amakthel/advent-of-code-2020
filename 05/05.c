#include <stdio.h>
#include <stdlib.h>

int main(){
  int c, i, s, d;
  int seats[884][2];
  int ids[884];
  char previous[10];
  char row[] = "       X";
  char col[] = "   X";
  char *trash;
  trash = (char*)malloc(10);

  i = s = d = 0;
  while ((c = getchar()) != EOF){
    if (c == '\n'){
      for (i=0;i<10;++i){
        if (previous[i] == 'F') previous[i] = '0';
        if (previous[i] == 'L') previous[i] = '0';
        if (previous[i] == 'B') previous[i] = '1';
        if (previous[i] == 'R') previous[i] = '1';
      }
      for (i=0;i<7;++i){
        row[i] = previous[i];
      }
      for (i=0;i<3;++i){
        col[i] = previous[i+7];
      }

      seats[s][0] = strtol(row, &trash, 2);
      seats[s][1] = strtol(col, &trash, 2);

      d = 0;
      ++s;
    }
    else {
      previous[d] = c;
      ++d;
    }
  }

  int high = 0;
  int low = 1024;
  int id;
  for (i=0;i<884;++i){
    id = ((seats[i][0])*8)+seats[i][1];
    ids[i] = id;
    if (id > high) high = id;
    if (id < low) low = id;
  }

  printf("record ids: %d %d\n", high, low);

  int checks[] = {0, 1, 0};
  int j;
  for (i=72;i<995;++i){
    for (j=0;j<884;++j){
      if((i-1) == ids[j]) checks[0] = 1;
      if(i == ids[j]) checks[1] = 0;
      if((i+1) == ids[j]) checks[2] = 1;
    }
    if((checks[0] == 1) && (checks[1] == 1) && (checks[2] == 1)) printf("your id, sir: %d\n", i);
    checks[0] = 0;
    checks[1] = 1;
    checks[2] = 0;
  }
}
