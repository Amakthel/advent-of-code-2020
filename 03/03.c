#include <stdio.h>
#include <stdlib.h>

int main(){
  int c, i, j, n;
  int totals[] = {0, 0, 0, 0, 0};
  char slope[31][323];
  int ishift[] = {1, 3, 5, 7, 1};
  int jshift[] = {1, 1, 1, 1, 2};

  i = j = 0;
  while ((c = getchar()) != EOF){
    if (c == '\n'){
      ++j;
      i = 0;
    }
    else {
      slope[i][j] = c;
      ++i;
    }
  }

  for (n=0;n<5;++n){
    i = j = 0;
    while (j<323){
      if (slope[i][j] == '#') ++totals[n];
      i = (i + ishift[n]) % 31;
      j = j + jshift[n];
    }
  }

  printf("first total: %d\n", totals[0]);
  printf("second total: %d\n", totals[1]);
  printf("third total: %d\n", totals[2]);
  printf("fourth total: %d\n", totals[3]);
  printf("fifth total: %d\n", totals[4]);
  
  return 0;
}
