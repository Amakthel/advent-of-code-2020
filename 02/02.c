#include <stdio.h>
#include <stdlib.h>

int main(){
  int total, sectotal, c,linc, i, min, max, ltot;
  char check;
  char *rest;
  char line[40];

  total = sectotal = c = linc = i = min = max = ltot = 0;
  for (i=0;i<40;++i){
    line[i] = ' ';
  }

  while ((c = getchar())!=EOF){
    if (c == '\n'){
      linc = 0;
      min = strtol(line, &rest, 10);
      for (i=0;i<39;++i){
        line[i] = rest[i+1];
      }
      max = strtol(line, &rest, 10);
      for (i=0;i<36;++i){
        line[i] = rest[i+1];
      }
      check = line[0];
      for (i=1;i<40;++i){
        if (line[i]==check) ++ltot;
      }
      if ((ltot <= max) && (ltot >= min)) ++total;
      if (((line[min+2] == check) && (line[max+2] != check)) || ((line[min+2] != check) && (line[max+2] == check))) ++sectotal;
      ltot = 0;
      for (i=0;i<40;++i){
        line[i] = ' ';
      }
    }
    else {
      line[linc] = c;
      ++linc;
    }
  }

  printf("there are %d passwords that satisfy pattern one\n", total);
  printf("there are %d passwords that satisfy pattern two\n", sectotal);
  
  return 0;
}
